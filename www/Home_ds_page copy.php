<?php
$dbUname="root";
$dbPword="1111";
$dbHost="db";
$dbName="database";

$con=mysqli_connect($dbHost,$dbUname,$dbPword,$dbName) or die("Error: " . mysqli_error($con));
          mysqli_query($con, "SET NAMES 'utf8' ");
          error_reporting( error_reporting() & ~E_NOTICE );
          date_default_timezone_set('Asia/Bangkok');  
?>      
<?php
    $sql_provinces = "SELECT * FROM provinces";
    $query = mysqli_query($con, $sql_provinces);
?>
      <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคเหนือ</span>
                <span class="info-box-number">
                  
                  <!-- <small>students</small> -->
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box  -->
            <!-- data recode -->
            
            <!-- /data recode -->
          </div>
          <!-- /.col -->
          
          <!-- teacher --><!-- teacher --><!-- teacher -->
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคตะวันออกเฉียงเหนือ</span>
                <!-- <span class="info-box-number">19 / 20 <small>Teacher</small></span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
                        <!-- data recode -->
                        
            <!-- /data recode -->
          </div>
          <!-- /.col -->
          <!-- /teacher --><!-- /teacher --><!-- /teacher -->
          
          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>
           
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคตะวันออก</span>
              </div>
            </div>
            <!-- /.info-box -->
                        <!-- data recode -->

            <!-- /data recode -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคกลาง</span>
                <!-- <span class="info-box-number">3 / 10 <small>Staff</small></span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
                        <!-- data recode -->

            <!-- /data recode -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-4"> 
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคใต้</span>
                <span class="info-box-number">151,750 (ลูกบาศก์เมตรต่อวัน)</span>
                <!-- <span class="info-box-number">3 / 10 <small>Staff</small></span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
                        <!-- data recode -->

            <!-- /data recode -->
          </div>
          <!-- /.col -->

          <div class="col-12 col-sm-6 col-md-4"> 
          <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-industry"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ภาคตะวันตก</span>
                <span class="info-box-number"></span>
                <!-- <span class="info-box-number">3 / 10 <small>Staff</small></span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-4"> 
            <!-- /.info-box -->
            <!-- data recode -->
            
            <!-- /data recode -->
          </div>
          <!-- /.col -->


          <div class="col-12 col-sm-12 col-md-8"> 
          <div class="row-12">
          <div class="info-box mb-8">
          
                    <div class="col-sm-4">
                      <!-- select -->
                      <div class="form-group">
                        <label>จังหวัด</label>

                  <select class="form-control" name="Ref_prov_id" id="provinces">
                  <option value="" selected disabled>-กรุณาเลือกจังหวัด-</option>
                  <?php foreach ($query as $value) { ?>
                  <option value="<?=$value['id']?>"><?=$value['name_th']?></option>
                  <?php } ?>
                    </select>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label>อำเภอ</label>
                        <select class="form-control" name="Ref_dist_id" id="amphures">
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label>ตำบล</label>
                        <select class="form-control" name="Ref_subdist_id" id="districts">
                        </select>
                      </div>
                    </div>
                
                  </div>  
                  <div class="info-box mb-8">
                    <div id="factory"></div>

                  </div>
                          
              <!-- /.info-box-content -->
            </div>

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <?php include('script1.php');?>
