<?php
  
require_once('connectdb.php');  

$id_f = $_GET["id_f"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BIOGAS</title>
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 
</head>
<style>
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: Gray;
   color: white;
   text-align: center;
}
</style>
<body>

<div class="jumbotron text-center card bg-666666" style="margin-bottom:0" >
  <h1>ระบบสารสนเทศ และฐานข้อมูลเพื่อติดตามและประเมินผลการผลิตก๊าชชีวิภาพในประเทศไทย</h1>
  </div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
         <a class="navbar-brand" href="#">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
 
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
              <li class="nav-item">
               <b><a class="nav-link" href="#">ความเป็นมาของโครงการ</a></b>
             </li>

            <li class="nav-item">
                    <b><a class="nav-link" href="#">ข้อมูลทั่วไป</a></b>
            </li>
            <li class="nav-item">
                     <b><a class="nav-link" href="total_gas_potentail.php">สรุปรวมกำลังผลิต</a></b>
             </li>

              <li class="nav-item">
                <b><a class="nav-link" href="totalThai_gas_potentail.php">ภาพรวมการผลิตทั้งประเทศ</a></b>
             </li>

        </ul>
  </div>
</nav>



<?php

        require_once('connectdb.php');

        $sql="SELECT * FROM `factory` WHERE id_f=  $id_f  ";

        if ($result=mysqli_query($con,$sql))
          {
          // Return the number of rows in result set
          $rowcount=mysqli_num_rows($result);
          // printf("Result set has %d rows.\n",$rowcount);
          // Free result set
          mysqli_free_result($result);
          }
?>

<div class="container" style="margin-top:30px">


<?php
 $sql  = "SELECT * FROM `factory`  WHERE id_f=  $id_f";
if ($result=mysqli_query($con,$sql))
 {
 $record =mysqli_fetch_array($result);
 mysqli_free_result($result);
 } ?>

<div class="container">
  <center><h1><b>ข้อมูลโรงงาน</b>  <?php echo $record['name_th'];?> </h1></center>
  <div class="card" style="width:100%">
   
    <div class="card-body">
      <h4 class="card-title"><b>ชื่อโรงงาน : </b> <?php echo $record['name_th'];?> </h4>
      <h4 class="card-title">เลขที่ : <?php echo $record['code_number'];?> </h4>
      <h4 class="card-title">หมู่ที่: <?php echo $record['moo_factory'];?> </h4>
      <h4 class="card-title">ตำบล : <?php echo $record['districts_id'];?> </h4>
      <h4 class="card-title">ภาค : <?php echo $record['name'];?> </h4>
      <h4 class="card-title">จังหวัด : <?php echo $record['name_th_p'];?> </h4>
      <h4 class="card-title">พิกัด : <?php echo $record['gps'];?> </h4>
      <h4 class="card-title">ผลิตภัณฑ์: <?php echo $record['product'];?> </h4>
      <h4 class="card-title">กำลังผลิต: <?php echo $record['capacity'];?> (ตัน/ปี) </h4>
      <h4 class="card-title">ผลปาล์มสดต่อปี : <?php echo $record['palm_peryear'];?> (ตัน/ปี)</h4>
      <h4 class="card-title">คำนวณน้ำเสีย: <?php echo $record['wastewater'];?> (ลบ.ม./ปี) </h4>
      <h4 class="card-title">ศักยภาพก๊าซคำนวณ : <?php echo $record['gas_potential'];?> (ล้าน ลบ.ม./ปี) </h4>
       
    </div>
  </div>
  <br>
</div>
<br><br><br><br><br><br>
</div>

<div class="footer">
<br>
<h4  class="w3-center" > Copyright © 2020 </h4>
<p class="w3-center" >King Mongkut's Institute of Technology Ladkrabang Prince of Chumphon Campus (KMITL PCC)</p>
</div>
</body>
</html>

