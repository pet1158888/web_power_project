$(document).ready(function(){
	var state_list=$("#state_list1")
	for (var state in simplemaps_countrymap_mapdata.state_specific){
		var key=state;
		var value=simplemaps_countrymap_mapdata.state_specific[state].name;
		 state_list.append($("<option></option>").attr("value",key).text(value)); 
	}						
	state_list.chosen();
	state_list.change(function(){
		var id=$(this).val();
		if(id=='THA375'){
			$.ajax({
				type: "POST",
				url: "ajax_db.php",
				data: {function:'districts'},
				success: function(data){
					$('#factory').html(data)
				}
			  });
			// $('#factory').html('ddd');
		}
		simplemaps_countrymap.state_zoom(id);
		
	});					
})

simplemaps_countrymap.hooks.back=function(){
	$("#state_list1").val(''); //When you zoom out, reset the select
	$("#state_list1").trigger("chosen:updated"); //update chosen
}
