ตัวแปรที่ใช้ในฟรอม reg_Student_page.php.php  {
student_code
class_code
program_code
Full_Name
Full_Name_Thai
Phone_Number
Id_card
Job_position
Date_enrolled
Address
Full_Name_Father
Phone_Number_Father
Full_Name_Mother
Phone_Number_Mother
Hospital
img_student
img_father
img_mother

ส่งข่อมูลไปยังไฟล์ script.php //save_student.php

} 
student_code sql{
    ID_No                             int(50)      null,
    Status                            varchar(255) null,
    Student_name                      varchar(255) null,
    Last_name                         varchar(255) null,
    First_name                        varchar(255) null,
    Thai_full_name                    varchar(255) null,
    Thai_last_name                    varchar(255) null,
    Thai_first_name                   varchar(255) null,
    Date_of_first_entry_at_TIS        varchar(255) null,
    Level                             varchar(255) null,
    Date_enrolled_for_the_school_year varchar(255) null,
    Birthday                          varchar(255) null,
    Gender                            varchar(255) null,
    Payor                             varchar(255) null,
    Home_phone_No                     varchar(255) null,
    Home_counrtry_phone               varchar(255) null,
    Student_passport_No               varchar(255) null,
    Nationality                       varchar(255) null,
    Thai_address                     varchar(255) null,
    Home_country_address              varchar(255) null,
    Alergies_or_medical_condition     varchar(255) null
}





       <?php        include('con.php');
                    $strSQL = "SELECT * FROM prefix WHERE keyname='year' ";
                    $objQuery = mysqli_query($conn,$strSQL) or die ("Error Query [".$strSQL."]");
                    while($objResult = mysqli_fetch_array($objQuery)){
                        $dateindata=$objResult["val"];
                        $val=$objResult["seq"];
                       
                
                    }
                    if($dateindata == date("Y"))
    {
        $dateshot = substr($dateindata,2);
        $Seq = substr("00".$val,-5,5);   //*** Replace Zero Fill ***//
        $strNextSeq = $dateshot.$Seq;
    
      
    }
    else  //*** Check val != year now ***//
    {
        $Seq = substr("01",-5,5);   //*** Replace Zero Fill ***//
        $strNextSeq = $dateshot.$Seq;
    
       
    }
    
    //return $strNextSeq;
  
?>

create table check_time_student
(
	id_no varchar(50) null,
	st_come timestamp null,
	st_out timestamp null,
	st_late varchar(50) null,
	st_leave varchar(50) null,
	st_absence varchar(50) null,
	st_total_money varchar(50) null
);

create table check_time_staff
(
	id_no varchar(50) null,
	sta_come timestamp null,
	sta_out timestamp null,
	sta_late varchar(50) null,
	sta_leave varchar(50) null,
	sta_absence varchar(50) null,
	sta_total_money varchar(50) null,
    sta_leave_over varchar(50) null,
    sta_goto_work varchar(50) null,
    sta_late_set timestamp,
    sta_late_Standard varchar(50) null
);
create table Teacher_reg
(
	id int null,
	ID_No varchar(50) null,
	Full_Name varchar(50) null,
	Full_Name_Thai varchar(50) null,
	Phone varchar(50) null,
	ID_Card varchar(50) null,
	Job_position varchar(50) null,
	Job_start varchar(50) null,
	Address varchar(150) null,
	img_teacher varchar(50) null,
	constraint Teacher_reg_pk
		primary key (id)
);

create table Staff_reg
(
	id int null,
	ID_No varchar(50) null,
	Full_Name varchar(50) null,
	Full_Name_Thai varchar(50) null,
	Phone varchar(50) null,
	ID_Card varchar(50) null,
	Job_position varchar(50) null,
	Job_start varchar(50) null,
	Address varchar(150) null,
	img_staff varchar(50) null,
	constraint Teacher_reg_pk
		primary key (id)
);

create table pickup_data
(
	row_id int auto_increment,
	ID_No varchar(20) null,
	Full_name1 varchar(50) null,
	Full_name2 varchar(50) null,
	Full_name3 varchar(50) null,
	Full_name4 varchar(50) null,
	Address1 varchar(200) null,
	Address2 varchar(200) null,
	Address3 varchar(200) null,
	Address4 varchar(200) null,
	Phone1 varchar(10) null,
	Phone2 varchar(10) null,
	Phone3 varchar(10) null,
	Phone4 varchar(10) null,
	Res_1 varchar(20) null,
	Res_2 varchar(20) null,
	Res_3 varchar(20) null,
	Res_4 varchar(20) null,
	img1 varchar(50) null,
	img2 varchar(50) null,
	img3 varchar(50) null,
	img4 varchar(50) null,
	constraint pickup_data_pk
		primary key (row_id)
);


create table student_detail
(
	row_id int auto_increment,
	ID_No varchar(50) null,
	Full_Name_father varchar(50) null,
	Occupation_father varchar(50) null,
	Email_father varchar(50) null,
	Address_Th_father varchar(200) null,
	Address_abroad_father varchar(200) null,
	Full_Name_mother varchar(50) null,
	Occupation_mother varchar(50) null,
	Email_mother varchar(50) null,
	Address_Th_mother varchar(200) null,
	Address_abroad_mother varchar(200) null,
	GUARDIAN_1 varchar(50) null,
	Occupation_1 varchar(50) null,
	Relationship_GUARDIAN_1 varchar(50) null,
	Address_GUARDIAN_1 varchar(200) null,
	GUARDIAN_2 varchar(50) null,
	Occupation_2 varchar(50) null,
	Relationship_GUARDIAN_2 varchar(50) null,
	Address_GUARDIAN_2 varchar(50) null,
	Insurance varchar(50) null,
	Types_of_Benefits varchar(50) null,
	How_to_Get varchar(50) null,
	Preferred_Hospital varchar(50) null,
	Phone_Hospital varchar(50) null,
	Doctor varchar(50) null,
	Phone_Doctor varchar(50) null,
	FAMILY1 varchar(50) null,
	Relationship_FAMILY1 varchar(50) null,
	FAMILY2 varchar(50) null,
	Relationship_FAMILY2 varchar(50) null,
	FAMILY3 varchar(50) null,
	Relationship_FAMILY3 varchar(50) null,
	FAMILY4 varchar(50) null,
	Relationship_FAMILY4 varchar(50) null,
	FINANCIAL varchar(50) null,
	Source_in varchar(50) null,
	Average varchar(50) null,
	Occupation_support varchar(50) null,
	Phone_support varchar(50) null,
	Email_support varchar(50) null,
	Address_support varchar(50) null,
	PAYMENT varchar(50) null,
	constraint student_detail_pk
		primary key (row_id)
);

create table login_user
(
	ID_No varchar(50) null,
	username varchar(50) null,
	password varchar(50) null,
	name varchar(50) null,
	constraint login_user_pk
		primary key (ID_No)
);

create table check_st_come
(
	row_id int null,
	date_user1 timestamp null,
	username1 varchar(50) null,
	date_user2 timestamp null,
	username2 varchar(50) null,
	date timestamp null,
	constraint check_st_come_pk
		primary key (row_id)
);

$_SESSION["user_name1"];
$_SESSION["user_name2"];
$_SESSION["name1"];
$_SESSION["name2"];


create table check_status_st
(
	ID_No varchar(50) null,
	status1 varchar(50) null,
	status2 varchar(50) null,
	status3 varchar(50) null,
	status4 varchar(50) null,
	status5 varchar(50) null,
	constraint check_status_st_pk
		primary key (ID_No)
);



       