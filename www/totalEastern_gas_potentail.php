<?php session_start();?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>BIOGAS</title>
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 
</head>
<style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: Gray;
   color: white;
   text-align: center;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  
}

td, th {
  border: 1px solid #dddddd;
  text-align : center;
  padding: 12px;
  font-size:160%;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<body>

<div class="jumbotron text-center card bg-666666" style="margin-bottom:0" >
  <h1>ระบบสารสนเทศ และฐานข้อมูลเพื่อติดตามและประเมินผลการผลิตก๊าชชีวิภาพในประเทศไทย</h1>
  </div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
         <a class="navbar-brand" href="#">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
 
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
              <li class="nav-item">
               <b><a class="nav-link" href="#">ความเป็นมาของโครงการ</a></b>
             </li>

            <li class="nav-item">
                    <b><a class="nav-link" href="#">ข้อมูลทั่วไป</a></b>
            </li>
            <li class="nav-item">
                     <b><a class="nav-link" href="total_gas_potentail.php">สรุปรวมกำลังผลิต</a></b>
             </li>

              <li class="nav-item">
                <b><a class="nav-link" href="totalThai_gas_potentail.php">ภาพรวมการผลิตทั้งประเทศ</a></b>
             </li>

        </ul>
  </div>
</nav>


<?php   
        require_once('connectdb.php'); 
        $sql  = "SELECT * FROM `factory` 
        JOIN geographies on geographies.id = factory.department
        WHERE department = 4";
//3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
$result = mysqli_query($con,$sql); ?>
<br><br>
<center><h1><b>ศักยภาพการผลิตและการนำก๊าซชีวภาพไปใช้เป็นพลังงานทดแทนของภาคตะวันออก</b></h1></center>

<br><br>

<h2><b>จำนวนจำนวนโรงงานที่ผ่านเกณฑ์ศักยภาพขั้นต่ำในภาคตะวันออก 10 โรงงาน</b></h2>
   <div class="w3-section">
      <table id="customers">
    <tr>
      <th>อันดับ</th>
      <th>ชื่อโรงงาน</th>
      <th>กำลังผลิต (ตัน/ปี)</th>
      <th>ผลปาล์มสดต่อปี (ตัน/ปี)</th>
      <th>รายละเอียดโรงงาน</th>
</tr>
<?php 
     while($record = mysqli_fetch_array($result)){ ?>
<tr>
    <td><?php echo $record['id_f'];?></td>
    <td><?php echo $record['name_th'];?> </td>
    <td><?php echo $record['capacity'];?>  (ตัน/ปี)</td>
    <td><?php echo $record['palm_peryear'];?>  (ตัน/ปี)</td>
    <td><a href = "formadd_datafactory.php?id_f=<?php echo $record ['id_f']; ?>">ดูข้อมูลเพิ่มเติม</a></td></tr>
    
<?php 
      }
?>

</table>

</div>

<br><br><br><br><br><br>
<div class="footer">
<br>
<h4  class="w3-center" > Copyright © 2020 </h4>
<p class="w3-center" >King Mongkut's Institute of Technology Ladkrabang Prince of Chumphon Campus (KMITL PCC)</p>
</div>
</body>
</html>

