
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BIOGAS</title>
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 
</head>
<style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: Gray;
   color: white;
   text-align: center;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  
}

td, th {
  border: 1px solid #dddddd;
  text-align : center;
  padding: 12px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<body>

<div class="jumbotron text-center card bg-666666" style="margin-bottom:0" >
  <h1>ระบบสารสนเทศ และฐานข้อมูลเพื่อติดตามและประเมินผลการผลิตก๊าชชีวิภาพในประเทศไทย</h1>
  </div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
         <a class="navbar-brand" href="#">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
 
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
              <li class="nav-item">
               <b><a class="nav-link" href="#">ความเป็นมาของโครงการ</a></b>
             </li>

            <li class="nav-item">
                    <b><a class="nav-link" href="#">ข้อมูลทั่วไป</a></b>
            </li>
            <li class="nav-item">
                     <b><a class="nav-link" href="total_gas_potentail.php">สรุปรวมกำลังผลิต</a></b>
             </li>

              <li class="nav-item">
                <b><a class="nav-link" href="#">ภาพรวมการผลิตทั้งประเทศ</a></b>
             </li>

        </ul>
  </div>
</nav>



<?php

        require_once('connectdb.php');

        $sql="SELECT * FROM `factory` WHERE 1 ";

        if ($result=mysqli_query($con,$sql))
          {
          // Return the number of rows in result set
          $rowcount=mysqli_num_rows($result);
          // printf("Result set has %d rows.\n",$rowcount);
          // Free result set
          mysqli_free_result($result);
          }
?>

<div class="container" style="margin-top:30px">


<?php
 $sql  = "SELECT * FROM `factory` WHERE 1 ";
if ($result=mysqli_query($con,$sql))
 {
 $record =mysqli_fetch_array($result);
 mysqli_free_result($result);
 } ?>

<center><h1><b>ศักยภาพการผลิตและการนำก๊าซชีวภาพไปใช้เป็นพลังงานทดแทนของประเทศไทย</b></h1></center>

<br><br><br>

<h2><b>จำนวนโรงงานในประเทศไทย</b></h2>
  <table >
  <tr>
    <th></th>
    <th>จำนวนโรงงาน</th>
    <th>จำนวนโรงงานที่ผ่านเกณฑ์ศักยภาพขั้นต่ำ</th>
  </tr>
  <tr>
    <td>เหนือ</td>
    <td>7</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ตะวันออกเฉียงเหนือ</td>
    <td>11</td>
    <td>22</td>
  </tr>
  <tr>
    <td>กลาง</td>
    <td>48</td>
    <td>14</td>
  </tr>
  <tr>
    <td>ใต้</td>
    <td>177</td>
    <td>77</td>
  </tr>
  <tr>
    <td>รวม</td>
    <td>265</td>
    <td>103</td>
  </tr>
 </table>


<br><br><br><br><br><br>
<div class="footer">
<br>
<h4  class="w3-center" > Copyright © 2020 </h4>
<p class="w3-center" >King Mongkut's Institute of Technology Ladkrabang Prince of Chumphon Campus (KMITL PCC)</p>
</div>
</body>
</html>

