
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BIOGAS</title>
  <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 
</head>
<style>
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: Gray;
   color: white;
   text-align: center;
}
</style>
<body>

<div class="jumbotron text-center card bg-666666" style="margin-bottom:0" >
  <h1>ระบบสารสนเทศ และฐานข้อมูลเพื่อติดตามและประเมินผลการผลิตก๊าชชีวิภาพในประเทศไทย</h1>
  </div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
         <a class="navbar-brand" href="#">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
 
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
              <li class="nav-item">
               <b><a class="nav-link" href="#">ความเป็นมาของโครงการ</a></b>
             </li>

            <li class="nav-item">
                    <b><a class="nav-link" href="#">ข้อมูลทั่วไป</a></b>
            </li>

            <li class="nav-item">
                     <b><a class="nav-link" href="#">สรุปรวมกำลังผลิต</a></b>
             </li>
             
              <li class="nav-item">
                <b><a class="nav-link" href="totalThai_gas_potentail.php">ภาพรวมการผลิตทั้งประเทศ</a></b>
             </li>

        </ul>
  </div>
</nav>



<?php

        require_once('connectdb.php');

        $sql="SELECT * FROM `factory` WHERE 1 ";

        if ($result=mysqli_query($con,$sql))
          {
          // Return the number of rows in result set
          $rowcount=mysqli_num_rows($result);
          // printf("Result set has %d rows.\n",$rowcount);
          // Free result set
          mysqli_free_result($result);
          }
?>

<div class="container" style="margin-top:30px">


<?php
 $sql  = "SELECT * FROM `factory` WHERE 1 ";
if ($result=mysqli_query($con,$sql))
 {
 $record =mysqli_fetch_array($result);
 mysqli_free_result($result);
 } ?>

<center><h1><b>ศักยภาพการผลิตและการนำก๊าซชีวภาพไปใช้เป็นพลังงานทดแทน</b></h1></center>

<br><br><br>

<div class="container">
  <center><h1><b>รวมทั้งประเทศ</b></h1></center>
  <div class="card" style="width:100%">
   
    <div class="card-body">
      <h4 class="card-title">กำลังผลิต: 16,457.262 (ตัน/ปี) </h4>
      <h4 class="card-title">ศักยภาพน้ำเสีย : 53,486,102 (ลบ.ม./ปี)</h4>
      <h4 class="card-title">ศักยภาพก๊าซ: 1,439.85 (ล้านลบ.ม./ปี) </h4>
    <br><h3><b>ศักยภาพเทียบเท่า</b></h3>
      <h4 class="card-title">น้ำมันดิบ: 715.765 (ktoe/ปี) </h4>
      <h4 class="card-title">พลังงานไฟฟ้า : 1,727.815 (ลบ.Wkh/ปี)</h4>
      <h4 class="card-title">ก๊าซชีวภาพอัด : 673,848 (ตัน/ปี) </h4>
    </div>
  </div>
  <br>
</div>

<div class="container">
  <center><h1><b>ภาคเหนือ</b></h1></center>
  <center><p><a href="totalNorth_gas_potentail.php" style="text-decoration: none">ดูรายละเอียด</a></p></></center>
  <div class="card" style="width:100%">
   
    <div class="card-body">
      <h4 class="card-title">กำลังผลิต: 0 (ตัน/ปี) </h4>
      <h4 class="card-title">ศักยภาพน้ำเสีย : 0 (ลบ.ม./ปี)</h4>
      <h4 class="card-title">ศักยภาพก๊าซ: 0 (ล้านลบ.ม./ปี) </h4>
    <br><h3><b>ศักยภาพเทียบเท่า</b></h3>
      <h4 class="card-title">น้ำมันดิบ: 0 (ktoe/ปี) </h4>
      <h4 class="card-title">พลังงานไฟฟ้า : 0 (ลบ.Wkh/ปี)</h4>
      <h4 class="card-title">ก๊าซชีวภาพอัด : 0 (ตัน/ปี) </h4>
    </div>
  </div>
  <br>
</div>

</div>

<div class="container">
  <center><h1><b>ภาคตะวันออกเฉียงเหนือ</b></h1></center>
  <center><p><a href="totalNorthEast_gas_potentail.php" style="text-decoration: none">ดูรายละเอียด</a></p></></center>
  <div class="card" style="width:100%">
   
  <div class="card-body">
      <h4 class="card-title">กำลังผลิต: 51,200 (ตัน/ปี) </h4>
      <h4 class="card-title">ศักยภาพน้ำเสีย : 166,400 (ลบ.ม./ปี)</h4>
      <h4 class="card-title">ศักยภาพก๊าซ: 4.479 (ล้านลบ.ม./ปี) </h4>
    <br><h3><b>ศักยภาพเทียบเท่า</b></h3>
      <h4 class="card-title">น้ำมันดิบ: 2.227 (ktoe/ปี) </h4>
      <h4 class="card-title">พลังงานไฟฟ้า : 5.375 (ลบ.Wkh/ปี)</h4>
      <h4 class="card-title">ก๊าซชีวภาพอัด : 2,096 (ตัน/ปี) </h4>
    </div>
  </div>
  <br>
</div>

</div>


<div class="container">
  <center><h1><b>ภาคกลาง</b></h1></center>
  <center><p><a href="totalCentral_gas_potentail.php" style="text-decoration: none">ดูรายละเอียด</a></p></></center>
  <div class="card" style="width:100%">
   
  <div class="card-body">
      <h4 class="card-title">กำลังผลิต: 659,040 (ตัน/ปี) </h4>
      <h4 class="card-title">ศักยภาพน้ำเสีย :2,141,880  (ลบ.ม./ปี)</h4>
      <h4 class="card-title">ศักยภาพก๊าซ: 57.659 (ล้านลบ.ม./ปี) </h4>
    <br><h3><b>ศักยภาพเทียบเท่า</b></h3>
      <h4 class="card-title">น้ำมันดิบ: 28.663 (ktoe/ปี) </h4>
      <h4 class="card-title">พลังงานไฟฟ้า : 69.191 (ลบ.Wkh/ปี)</h4>
      <h4 class="card-title">ก๊าซชีวภาพอัด : 26,985 (ตัน/ปี) </h4>
    </div>
  </div>
  <br>
</div>
</div>



<div class="container">
  <center><h1><b>ภาคใต้ <br></h1></center>
  <center><p><a href="totalSouth_gas_potentail.php" style="text-decoration: none">ดูรายละเอียด</a></p></></center>
  <div class="card" style="width:100%">
   
  <div class="card-body">
      <h4 class="card-title">กำลังผลิต: 16,457,262 (ตัน/ปี) </h4>
      <h4 class="card-title">ศักยภาพน้ำเสีย : 53,486,102 (ลบ.ม./ปี)</h4>
      <h4 class="card-title">ศักยภาพก๊าซ: 261.120 (ล้านลบ.ม./ปี) </h4>
    <br><h3><b>ศักยภาพเทียบเท่า</b></h3>
      <h4 class="card-title">น้ำมันดิบ: 129.806 (ktoe/ปี) </h4>
      <h4 class="card-title">พลังงานไฟฟ้า : 313.344 (ลบ.Wkh/ปี)</h4>
      <h4 class="card-title">ก๊าซชีวภาพอัด : 122,204 (ตัน/ปี) </h4>
    </div>

  </div>
  <br>
</div>
</div>


<br><br><br><br><br><br>
<div class="footer">
<br>
<h4  class="w3-center" > Copyright © 2020 </h4>
<p class="w3-center" >King Mongkut's Institute of Technology Ladkrabang Prince of Chumphon Campus (KMITL PCC)</p>
</div>
</body>
</html>

